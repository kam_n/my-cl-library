(defpackage #:tomov.kamen.logging
  (:use #:cl)
  (:export #:log-debug #:log-info #:log-error)
  (:documentation "Simple logging"))

(in-package #:tomov.kamen.logging)

(defvar +debug+ -5)
(defvar +info+ 0)
(defvar +error+ 10)

(defvar +default-log-level+ +info+)

(defun format-date (date)
  (multiple-value-bind (second minute hour)
      (decode-universal-time date)
    (format nil "~2,'0d:~2,'0d:~2,'0d" hour minute second)))

(defmacro log-it (level datum args)
  `(when (>= ,level +default-log-level+)
     (apply
      #'format t(concatenate 'string "~a ~a: " ,datum "~%")
      ,(append `(list (format-date (get-universal-time))
                      ,(symbol-name level))
               args))))

(defmacro log-debug (datum &rest args)
  `(log-it +debug+ ,datum ,args))

(defmacro log-info (datum &rest args)
  `(log-it +info+ ,datum ,args))

(defmacro log-error (datum &rest args)
  `(log-it +error+ ,datum ,args))
