(asdf:defsystem #:lib
  :description "Common libraries"
  :author "Kamen Tomov"
  :version "0.0.2"
  :serial t
  :depends-on (yason)
  :components ((:module "logging"
                        :components ((:file "logging")))
               (:module "process-manipulation"
                        :components ((:file "process-manipulation")))
               (:module "os"
                        :components ((:file "os")))
               (:module "kamo-lib"
                        :components ((:file "packages")
                                     (:file "kamo-lib" :depends-on ("packages"))
                                     (:file "pg-lib" :depends-on ("packages"))))))
