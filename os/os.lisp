(defpackage #:tomov.kamen.os
  (:use #:cl #:tomov.kamen.process-manipulation #:tomov.kamen.logging)
  (:export #:run-aws #:invoke-process #:collect-process-outcomes))

(in-package #:tomov.kamen.os)

(defvar *testp* nil)
(setq *testp* nil)

(defstruct command-sting
  (val "" :type string))

(defun set-command-string (command-string app args)
  (setf (command-sting-val command-string)
        (format
         nil
         "~{~a ~}"
         (append
          (unless (equal "" (command-sting-val command-string))
            (list (command-sting-val command-string)))
          (cons app args)))))

(defun run-rm (cmd-str args wait)
  (let ((app "rm"))
    (set-command-string cmd-str app args)
    (run-program
     app
     args
     :wait wait
     :search
     :input nil
     :output :stream)))

(defun run-rmdir (cmd-str args wait)
  (let ((app "rmdir"))
    (set-command-string cmd-str app args)
    (run-program
     app
     args
     :wait wait
     :search
     :input nil
     :output :stream)))

(defun get-test-args ()
  (list
   (with-output-to-string (var)
     (yason:with-output (var)
       (yason:with-object ()
         (yason:encode-object-element "location" "yo")
         (yason:encode-object-element
          "checksum"
          "125454bf6905f90dac6880d9bbccccd6942960930a68ea052dfc464a6ec9978e")
         (yason:encode-object-element "archiveId" "yoaaa")
         (yason:encode-object-element "uploadId" "yoaaa2"))))))

(defun run-aws (cmd-str args wait)
  (let ((app "aws")
        (args (append
               (list "--output" "json" "--region" (third args) "glacier" (first args)
                     "--account-id" "-" "--vault-name" (second args))
               (cdddr args))))
    (set-command-string cmd-str app args)
    (if *testp*
        (setf app "echo"
              args (get-test-args)))
    (run-program
     app
     args
     :output :stream
     :input nil
     :wait wait
     :search t)))

(defun invoke-process (run-f run-args process-f &optional process-args)
  "Invokes a process and returns the result of fn on it."
  (let* ((cmd-string (make-command-sting))
         (process (funcall run-f cmd-string run-args nil)))
    (log-debug "~a" (command-sting-val cmd-string))
    (unwind-protect (funcall process-f process process-args)
      (process-close process))))

(defun collect-process-outcomes (process)
  (with-open-stream (stream (process-output process))
    (loop for line = (read-line stream nil)
       while line
       collect line)))
