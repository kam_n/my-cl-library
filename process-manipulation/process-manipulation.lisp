(defpackage #:tomov.kamen.process-manipulation
  (:use #:cl)
  (:export #:process-wait #:run-program #:process-output
           #:process-exit-code #:process-close #:quit
           #:process-condition #:process-condition-error
           #:extract-environment-params
           #:getenv))

(in-package :tomov.kamen.process-manipulation)

;sb-ext:string-to-octets
;sb-ext:octets-to-string

(defun process-wait (process &optional check-for-stopped)
  #+sbcl
  (sb-ext:process-wait process check-for-stopped)
  #+ecl
  (ext:external-process-wait process t)
  #-(or
     sbcl
     ecl)
  (not-implemented 'process-wait))

(defun run-program (program args &key search wait input output)
  #+sbcl
  (sb-ext:run-program program args :search search :wait wait :input input :output output)
  #+ecl
  (nth-value 2 (ext:run-program program args :wait wait :input input :output output))
  #-(or
     sbcl
     ecl)
  (not-implemented 'run-program))

(defun process-output (process)
  #+sbcl
  (sb-ext:process-output process)
  #+ccl
  (ccl:external-process-output-stream process)
  #+ecl
  (ext:external-process-output process)
  #-(or
     sbcl
     ccl
     ecl)
  (not-implemented 'process-output))

(defun process-exit-code (process)
  #+sbcl
  (sb-ext:process-exit-code process)
  #+ccl
  (multiple-value-bind (status code) (ccl:external-process-status process)
    (when (eq :exited status)
      code))
  #+ecl
  (multiple-value-bind (status code) (ext:external-process-status process)
    (when (eq :exited status)
      code))
  #-(or
     sbcl
     ccl
     ecl)
  (not-implemented 'process-exit-code))

; check if the streams need to be closed for ECL
(defun process-close (process)
  #+sbcl
  (sb-ext:process-close process)
  #+ecl
  process
  #-(or
     sbcl
     ecl)
  (not-implemented 'process-close))

(defun quit (&key recklessly-p (unix-status 0))
  #+sbcl
  (sb-ext:quit :recklessly-p recklessly-p :unix-status unix-status)
  #+ecl
  (ext:quit unix-status)
  #-(or
     sbcl
     ecl)
  (not-implemented 'quit))

(define-condition process-condition ()
  ((code :initarg :code :reader process-condition-code)
   (message :initarg :error :reader process-condition-error)))

(defmethod print-object ((object process-condition) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "process-condition: ~A: ~A" (process-condition-code object) (process-condition-error object))))

(define-condition missing-env-var ()
  ((name :initarg :name :reader missing-env-var-name)))

(defmethod print-object ((object missing-env-var) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "(~A)" (missing-env-var-name object))))

(defun getenv (name &optional default)
  "Obtains the current value of the POSIX environment variable NAME."
  (declare (type (or string symbol) name))
  (let ((name (string name)))
    (or #+abcl (ext:getenv name)
        #+ccl (ccl:getenv name)
        #+clisp (ext:getenv name)
        #+cmu (unix:unix-getenv name)   ; since CMUCL 20b
        #+ecl (si:getenv name)
        #+gcl (si:getenv name)
        #+mkcl (mkcl:getenv name)
        #+allegro (sys:getenv name)
        #+lispworks (lispworks:environment-variable name)
        #+sbcl (sb-ext:posix-getenv name)
        default)))

(defun extract-environment-params (lst)
  (loop for name in lst
     collect (cons name (let ((value (getenv (symbol-name name))))
                          (or value (error (make-condition 'missing-env-var :name name)))))))
