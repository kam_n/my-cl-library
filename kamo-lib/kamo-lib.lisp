;;;;
;;;; Copyright (c) 2015 Kamen Tomov, All Rights Reserved
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;;
;;;;   * Redistributions of source code must retain the above copyright
;;;;     notice, this list of conditions and the following disclaimer.
;;;;
;;;;   * Redistributions in binary form must reproduce the above
;;;;     copyright notice, this list of conditions and the following
;;;;     disclaimer in the documentation and/or other materials
;;;;     provided with the distribution.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;;

;;;; kamo-lib.lisp

(in-package :kamo-lib)

(defun gen-typespec (lambda-list)
  `(,@(mapcan
       #'(lambda (el)
           (if (consp el)
               `((declare ,(reverse el)))))
       lambda-list)))

(defmacro if-bind ((var expr) then &optional else)
  `(let ((,var ,expr))
     (if ,var
       ,then
       ,else)))

(defun simple-rule-of-3 (lst)
  "Calculates a value using the simple rule of three. Expects a list of four elements, one of which is not a number."
  (multiple-value-bind (1st 2nd 3rd 4th) (values-list lst)
    (let* ((found? t)
           (prod (* 1.0
                    (if (numberp 1st)
                        (if (numberp 4th)
                            (progn (setq found? nil)
                                   (* 1st 4th))
                            1st)
                        4th))))
      (if found?
          (/ (* 2nd 3rd) prod)
          (/ prod (if (numberp 2nd) 2nd 3rd))))))

(defmacro letif* (bindings &body body)
  (labels ((letifun (lst &rest body)
             (if lst
                 (let* ((el (car lst))
                        (var (car el)))
                   (with-gensyms (init)
                     `(let* ((,init ,(second el))
                             (,var ,init))
                        (if ,var
                            ,(letifun (cdr lst) (car body))
                            ,(third el)))))
                 `(progn ,@(car body)))))
    (letifun bindings body)))

(defun logwrite (where proc destination source &rest params)
  (with-open-file (ss "/tmp/output" :direction :output :if-does-not-exist :create :if-exists :append)
    (multiple-value-bind (s m)  (decode-universal-time (get-universal-time))
      (format ss "~%~a:~a(~a) proc:~a dst:~a src:~a params:~a ~%"
              m s where proc destination source params))))

(defun concatenate-list (lst &optional (delimiter #\Space))
  (with-output-to-string (str)
    (mapcar #'(lambda (el)
                (princ el str)
                (princ delimiter str))
            lst)
    str))

(defun vector-to-string (ip &optional (delimiter "."))
  (subseq (with-output-to-string (s)
            (dotimes (ix (length ip))
              (princ delimiter s)
              (princ (aref ip ix) s)))
          1))

(defun last-1 (lst &optional one two flag)
  "Returns the list element before the last one."
  (if lst
      (if flag
          (last-1 (cdr lst) one (car lst) (not flag))
          (last-1 (cdr lst) (car lst) two (not flag)))
      (if flag two one)))

(defun handle-whitespace (str repl-ch)
  (map 'string
       #'(lambda (ch)
           (case ch
             ((#\Return #\Tab #\Space #\Linefeed #\Newline) repl-ch)
             (t ch)))
       str))

(defun position-str (str sequence)
  (let ((len (length str)))
    (loop for i to (- (length sequence) len)
       when (equal str (subseq sequence i (+ i len))) return i)))

(defgeneric get-bday-from-egn (egn)
  (:documentation "Extracts the bday from the EGN"))

(defmethod get-bday-from-egn ((egn string))
  (encode-universal-time
   0 0 0
   (parse-integer (subseq egn 4 6))
   (parse-integer (subseq egn 2 4))
   (let ((num (parse-integer (subseq egn 0 2))))
     (+ (if (< num 10) 2000 1900) num))))

(defmethod get-bday-from-egn ((egn integer))
  (multiple-value-bind (yr r) (floor egn 100000000)
    (multiple-value-bind (mo r) (floor r 1000000)
      (encode-universal-time
       0 0 0 (floor r 10000) mo (+ (if (< yr 10) 2000 1900) yr)))))

(defun chop-str (str &optional (len 1))
  (subseq str 0 (- (length str) len)))

(defun parse-float (lst)
  "Parse float in a form (mantice remainder)"
  (let* ((int (first lst))
         (fp (second lst)))
    (+ (parse-integer int) 0.0
       (if fp (/ (parse-integer fp) (expt 10 (length fp))) 0))))

(defun split-by-one-char (string &optional (chr #\Space))
  (loop for el in (loop for i = 0 then (1+ j)
                        as j = (position chr string :start i)
                        collect (subseq string i j)
                        while j)
        when (and (not (zerop (length el)))) collect el))

(defun to-camel-case (str)
  (format nil "~@(~a~)" str))

(defun to-upper-case (str)
  (map 'string #'char-upcase str))

(defun slot-value-or-nil (obj slot-name)
  (and (slot-boundp obj slot-name)
       (slot-value obj slot-name)))

(defmacro d-value (description value)
  (declare (ignore description))
  value)

(defun determinant (matrix)
  "Finds the determinant of 3x3 matrix. (make-array '(3 3) :initial-contents '((1 20 1) (2 50 1) (1 30 1)))"
  (let ((a (aref matrix 0 0))
        (b (aref matrix 0 1))
        (c (aref matrix 0 2))
        (d (aref matrix 1 0))
        (e (aref matrix 1 1))
        (f (aref matrix 1 2))
        (g (aref matrix 2 0))
        (h (aref matrix 2 1))
        (i (aref matrix 2 2)))
    (+ (- (* a (- (* e i) (* h f)))
          (* d (- (* b i) (* h c))))
       (* g (- (* b f) (* e c))))))

(defun determinant-3 (a b c y1 y2 y3)
  "Finds the determinant of 3x3 matrix with last row of 1s. http://mathworld.wolfram.com/Circumcircle.html"
    (+ (* a (- y2 y3))
       (* b (- y3 y1))
       (* c (- y1 y2))))


(defun get-circumcenter (lst)
  (let ((len (length lst)))
    (cond
      ((= 3 len) (multiple-value-bind (t1 t2 t3) (values-list lst)
                   (let ((x1 (car t1))
                         (y1 (cdr t1))
                         (x2 (car t2))
                         (y2 (cdr t2))
                         (x3 (car t3))
                         (y3 (cdr t3)))
                     (let ((a (+ (* x1 x1) (* y1 y1)))
                           (b (+ (* x2 x2) (* y2 y2)))
                           (c (+ (* x3 x3) (* y3 y3))))
                       (let ((alpha (determinant-3 x1 x2 x3 y1 y2 y3))
                             (bx (- (determinant-3 a b c y1 y2 y3)))
                             (by (determinant-3 a b c x1 x2 x3)))
                         (format t "alpha: ~s, bx: ~s, by: ~s" alpha bx by)
                         (cons (/ bx -2 alpha) (/ by -2 alpha))))))))))

(defmacro define-constant (name value &optional doc)
  `(defconstant ,name (if (boundp ',name) (symbol-value ',name) ,value)
                      ,@(when doc (list doc))))

(defun replace-all (string part replacement &key (test #'char=))
"Returns a new string in which all the occurences of the part
is replaced with replacement."
    (with-output-to-string (out)
      (loop with part-length = (length part)
            for old-pos = 0 then (+ pos part-length)
            for pos = (search part string
                              :start2 old-pos
                              :test test)
            do (write-string string out
                             :start old-pos
                             :end (or pos (length string)))
            when pos do (write-string replacement out)
            while pos)))
